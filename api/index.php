<?php
ini_set('memory_limit', '200M');

include __DIR__.'/../simple_html_dom.php';
include __DIR__.'/../simple_image.php';
include __DIR__.'/../Db.php';

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();

$app['debug'] = true;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   	=> 'pdo_mysql',
        'host'     	=> 'localhost',
		'user'		=> 'root',
		'password'	=> '',
		'dbname'	=> 'avito',
		'charset'	=> 'utf8'
    ),
));

class Avito{
    
    public $host = 'https://m.avito.ru';
    public $regionsSearch = 'ul.list a';
    public $citySearch = 'ul.list a';
    public $advertSearch = 'article';
    
}

$avito = new Avito;

$app->get('/regions', function() use($avito) {
    
    $html = file_get_html($avito->host);
    $regions = $html->find($avito->regionsSearch);
    
    foreach($regions as $item){
        $post[] = ['title'=>$item->innerText(),'id'=>$item->href];
    }
    
    return json_encode($post);
});

$app->get('/get/{path}/{path2}', function($path, $path2) use($avito) {
    
    $html = file_get_html($avito->host.'/'.$path.'/'.$path2);
    $res = $html->find($avito->citySearch);
    
    foreach($res as $item){
        $post[] = ['title'=>$item->innerText(),'id'=>$item->href];
    }
    
    return json_encode($post);
});

$app->get('/get/{path}/{path2}/{path3}', function($path, $path2, $path3) use($avito) {
    
    $html = file_get_html($avito->host.'/'.$path.'/'.$path2.'/'.$path3);
    $res = $html->find($avito->citySearch);
    
    foreach($res as $item){
        $post[] = ['title'=>$item->innerText(),'id'=>$item->href];
    }
    
    return json_encode($post);
});

$app->get('/art/{path}/{path2}', function($path, $path2) use($avito) {
    
    $html = file_get_html($avito->host.'/'.$path.'/'.$path2);
    $res = $html->find($avito->advertSearch);
    
    foreach($res as $item){
        $post[] = ['price'=>$item->find('.item-price')[0].innerText(), 'img'=>$item->find('.pseudo-img')[0]->style,'title'=>$item->find('h3')[0]->innerText(),'id'=>$item->find('a.item-link')->href];
    }
    
    return json_encode($post);
});

/*

$app->put('/recipes/{id}', function (Request $request, $id) use ($app) {

	$put = [
        'name' => $request->request->get('name'),
		'descr' => $request->request->get('descr'),
		'img' => $request->request->get('img'),
		'time' => $request->request->get('time'),
    ];

	$sql = "UPDATE recipes SET name = ?, descr = ?, img = ?, time = ? WHERE id = ?";
	$res = $app['db']->executeUpdate($sql, array($put['name'], $put['descr'], $put['img'], $put['time'],(int) $id));
	if($res)
		return json_encode($res);
});

$app->get('/tags/recipe/{recipeid}', function(Silex\Application $app, $recipeid) use($app) {
	$sql = "SELECT t.* FROM tags t INNER JOIN recipes_tags r ON r.tag_id = t.id WHERE r.recipe_id = ?";
    $res = $app['db']->fetchAll($sql, [(int) $recipeid]);
	if (empty($res)) {
        $app->abort(404, "Recipeid {$recipeid} does not exist.");
    }
    return json_encode($res);
});

$app->get('/', function() {
    return 'API recipes';
});

$app->post('/toys', function (Silex\Application $app) use ($toys) {
   //...
});

$app->put('/{stockcode}', function (Silex\Application $app, $stockcode) use ($toys) {
   //...
});

$app->delete('/{stockcode}', function (Silex\Application $app, $stockcode) use ($toys) {
   //...
});
*/
$app->run();

