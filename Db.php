<?php

class Db {
    
    private $dbh;
    private $query;
    
    public function __construct() {
        
        $this->dbh = new PDO('mysql:host=localhost;dbname=avito', 'root', '');
        $this->dbh->exec("set names utf8");
        
    }
    
    public function query($sql)
    {
        $this->query = $this->dbh->prepare($sql);
        return $this;
    }
    
    public function params($params)
    {
        foreach ($params as $key => &$value) {
            if(is_int($key))
                $this->query->bindParam(($key + 1), $value);
            else
                $this->query->bindParam($key, $value);  
        }
        return $this;
    }
    
    public function result($fetchMode = PDO::FETCH_OBJ)
    {
        $this->query->execute();
        $this->query->setFetchMode($fetchMode);
        
        $result = [];
        
        while($row = $this->query->fetch())
        {
            $result[] = $row;
        }
		
		if(empty($result))
			$result = $this->dbh->lastInsertId();
        
        return $result;
    }

}
